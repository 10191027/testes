package com.fanny_10191027.dashboard;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class DashBoardAdapter extends RecyclerView.Adapter<DashBoardAdapter.DashBoardHolder> {

    private ArrayList<SetterGetter> lisdata;

    public DashBoardAdapter(ArrayList<SetterGetter> lisdata){
        this.lisdata    = lisdata;
    }
    @NonNull
    @NotNull
    @Override
    public DashBoardHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view               = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard,parent,false);
        DashBoardHolder holder  = new DashBoardHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull DashBoardAdapter.DashBoardHolder holder, int position) {

        final SetterGetter getData  = lisdata.get(position);
        String titlemenu            = getData.getTitle();
        String logomenu             = getData.getImg();

        holder.titleMenu.setText(titlemenu);
        if (logomenu.equals("logomenu1")){
            holder.imgMenu.setImageResource(R.drawable.plane);
        }else if (logomenu.equals("logomenu2")){
            holder.imgMenu.setImageResource(R.drawable.hotel);
        }if (logomenu.equals("logomenu3")){
            holder.imgMenu.setImageResource(R.drawable.spa);
        }if (logomenu.equals("logomenu4")){
            holder.imgMenu.setImageResource(R.drawable.train);
        }if (logomenu.equals("logomenu5")){
            holder.imgMenu.setImageResource(R.drawable.car);
        }if (logomenu.equals("logomenu6")){
            holder.imgMenu.setImageResource(R.drawable.health);
        }if (logomenu.equals("logomenu7")){
            holder.imgMenu.setImageResource(R.drawable.villa);
        }if (logomenu.equals("logomenu8")){
            holder.imgMenu.setImageResource(R.drawable.bus);
        }if (logomenu.equals("logomenu9")){
            holder.imgMenu.setImageResource(R.drawable.dine);
        }
    }

    @Override
    public int getItemCount() {
        return lisdata.size();
    }

    public class DashBoardHolder extends RecyclerView.ViewHolder {

        TextView titleMenu;
        ImageView imgMenu;
        public DashBoardHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            titleMenu   = itemView.findViewById(R.id.tv_title_menu);
            imgMenu     = itemView.findViewById(R.id.iv_logo_menu);
        }
    }
}
