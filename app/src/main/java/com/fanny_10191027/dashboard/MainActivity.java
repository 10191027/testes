package com.fanny_10191027.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<SetterGetter> datamenu;
    GridLayoutManager gridLayoutManager ;
    DashBoardAdapter adapter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = findViewById(R.id.rv_menu);

        addData();
        gridLayoutManager   = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new DashBoardAdapter(datamenu);
        recyclerView.setAdapter(adapter);

    }



    public void addData(){

        datamenu    = new ArrayList<>();
        datamenu.add(new SetterGetter("Tiket Pesawat", "logomenu1"));
        datamenu.add(new SetterGetter("Hotel", "logomenu2"));
        datamenu.add(new SetterGetter("Spa & Kecantikan", "logomenu3"));
        datamenu.add(new SetterGetter("Tiket Kereta Api", "logomenu4"));
        datamenu.add(new SetterGetter("Mobil", "logomenu5"));
        datamenu.add(new SetterGetter("Helath", "logomenu6"));
        datamenu.add(new SetterGetter("Vila & Apartemen", "logomenu7"));
        datamenu.add(new SetterGetter("Tiket Bus & Travel", "logomenu8"));
        datamenu.add(new SetterGetter("Dine-In", "logomenu9"));





    }
}